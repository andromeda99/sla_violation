# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 13:18:22 2019

@author: 45041313
"""

import csv
from datetime import timedelta
from dateutil import parser
df = pd.DataFrame(dict) 
   
with open('Documents\sla_violation\jira.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count_correct = 0
    line_count_write = 0
    line_count_incorrect = 0
    total_sla_breached = 0
    status = 0
    for row in csv_reader:
        x=(row["Created"])
        y=(row["Resolved"])
        z=(row["Key"])
        s=(row["Status"])
        a=(row["Assignee"])
        try:
            parsedx = parser.parse(x)
            parsedy = parser.parse(y)
            daygenerator = (parsedx + timedelta(x + 1) for x in range((parsedy - parsedx).days))
            working_days = sum(1 for day in daygenerator if day.weekday() < 5)
            if working_days > 5:
                print("SLA Breached : ", z,s,a)
                dict = {'Key':[z], 
                        'Status': [s], 
                        'SLA':["yes"]}
                total_sla_breached += 1
                status += 1
            line_count_correct += 1
        except:
            print("Pending Tickets : ", z,s,a)
            dict = {'Key':[z], 
                        'Status': [s], 
                        'SLA':["NA"]}
            status += 1
            line_count_incorrect += 1
            
    print(f'\nProcessed {line_count_correct} lines correctly.\n')
    print(f'Processed {line_count_incorrect} lines incorrectly.\n')
    print(f'Processed {line_count_incorrect+line_count_correct} lines totally.\n')
    print(f'Total SLA Breached : {total_sla_breached}\n')
    print(df)